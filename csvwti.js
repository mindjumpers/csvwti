/*eslint-env node es6 */
/*eslint-dont-disable no-console */
'use strict';


// actual constants
const ARG_START = 2;
const HTTP_NOT_FOUND = 404;
const FETCH_TOKEN_COST = 1;

const LOG_LEVELS = {
	emergency: 9, alert: 8, critical: 7, error: 6, warning: 5, warn: 5, notice: 4, log: 3, info: 2, debug: 1
};
const WTI_BASE_URL = 'https://webtranslateit.com/api/projects/';

// WTIs API rate limit is 15/second - we settle for 14; see https://webtranslateit.com/en/docs/api
const WTI_RATE_LIMIT_QUANTITY = 14;
// can be 'second', 'minute', 'hour' or 'day'
const WTI_RATE_LIMIT_UNIT = 'second';


// system requires
const fs = require( 'fs' );
const util = require( 'util' );

// external requires
const async = require( 'async' );
const csvReader = require( 'csv-streamify' );
const csvWriter = require( 'csv-write-stream' );
const minimist = require( 'minimist' );
const limiterModule = require( 'limiter' );
const request = require( 'request' );

// module variables
const argv = minimist( process.argv.slice( ARG_START ) );
const limiter = new limiterModule.RateLimiter( WTI_RATE_LIMIT_QUANTITY, WTI_RATE_LIMIT_UNIT );
const missingLangs = {};
const projectLangs = {};
const parser = csvReader( { objectMode: true } );

let langs = false;
let logMinimumLevel = 'debug';
let missingLog = false;
let errorLog = false;
let wtiApiUrl;

function isFunction( functionToCheck ) {
	const getType = {};
	return functionToCheck &&
		getType.toString.call( functionToCheck ) === '[object Function]';
}


function usage() {
	process.stdout.write( 'Usage: node csvwti.js --project #key# --input #csvfile# [ --missing #csvfile# ]\n\nSee README.md for more info.\n' );
	process.exit();
}

function checkArgv() {
	const missing = [];
	log( 'debug', 'Check argv', argv );
	if( argv[ '?' ] || argv.help ) {
		usage();
	}

	if( ! argv.project ) {
		missing.push( 'project' );
	}
	if( ! argv.input ) {
		missing.push( 'input' );
	}
	if( missing.length ) {
		log( 'error', 'Missing argument(s):', missing.join( ', ' ) );
		usage();
	}
}

function log( level ) {
	const argumentsArray = [].slice.apply( arguments );
	if( ! LOG_LEVELS[ level ] ) {
		argumentsArray.unshift( 'info' );
	}

	const lcLevel = argumentsArray[ 0 ].toLowerCase();

	argumentsArray[ 0 ] = argumentsArray[ 0 ].toUpperCase();

	if( errorLog ) {
		errorLog.write( '' + new Date() );
		errorLog.write( JSON.stringify( argumentsArray ) + '\n' );
	}

	if( LOG_LEVELS[ lcLevel ] < LOG_LEVELS[ logMinimumLevel ] ) {
		return;
	}


	util.log.apply( util, argumentsArray );
}


function wtiFetch( name, qs, method, callback ) {
	const o = {
		method: method,
		uri: wtiApiUrl + name + '.json',
		json: true
	};

	switch( method ) {
	case 'GET':
		o.qs = qs;
		break;
	default:
		o.body = qs;
	}

	limiter.removeTokens( FETCH_TOKEN_COST, performRequest );

	function performRequest() {
		request( o, function requestResult( e, r, body ){
			if( e ) {
				log( 'error', 'Got WTI API error:', e, r, body );
				process.exit();
			}

			log( 'debug', r.statusCode, name );

			callback( body, r.statusCode );
		} );
	}
}

function createBatch( key, stringId, translations ) {
	const batch = [];
	for( let i = 0; i < langs.length && i < translations.length; i += 1 ) {
		if( ! missingLangs[ langs[ i ] ] ) {
			batch.push( { key: key, stringId: stringId, lang: langs[ i ], translation: translations[ i ] } );
		}
	}
	return batch;
}

function processTask( task, callback ) {
	const q = '/strings/' + task.stringId +
		'/locales/' + task.lang +
		'/translations';

	wtiFetch( q, {}, 'GET', receiveString );

	function doCallback(){
		if( isFunction( callback ) ) {
			callback();
		}
	}

	function receiveString( b, statusCode ){
		log( 'debug', 'read:', statusCode, task, b && b.text );

		if( statusCode === HTTP_NOT_FOUND ) {
			log( 'error', 'String not found - ignored', task );
			doCallback();
			return;
		}

		if( b && b.text === task.translation ) {
			log( 'info', 'no change needed - ignored', task.key, task );
			doCallback();
		} else {
			wtiFetch( q, { text: task.translation }, 'POST', updateResult );
		}
	}

	function updateResult( b, statusCode ) {
		if( statusCode >= 400 ) {
			log( 'error', 'failed to write:', statusCode, task, b && b.text );
		} else {
			log( 'info', 'wrote:', statusCode, task, b && b.text );
		}
		doCallback();
	}
}

function arrayClonePrepend( prepend, arr ) {
	const retval = arr.slice();
	retval.unshift( prepend );
	return retval;
}

function processString( key, translations ) {

	wtiFetch(
		'/strings', { 'filters[key]': key }, 'GET',
		processStringResult
	);

	function processStringResult( stringRes ){
		if( ! stringRes.length || ! stringRes[ 0 ].id ) {
			if( missingLog ) {
				missingLog.write( arrayClonePrepend( key, translations ) );
			}
			log( 'warn', 'String', key, 'NOT found - ignored.' );
			return;
		}

		log( 'debug', 'found string', key, stringRes[ 0 ].id );

		const stringId = stringRes[ 0 ].id;
		const batch = createBatch( key, stringId, translations );
		const sourceTask = batch.shift();

		processTask( sourceTask, sourceTaskDone );

		function sourceTaskDone(){
			async.each( batch, processTask );
		}
	}
}

function receiveLine( line ) {
	// log( 'debug', 'read line from CSV', line );

	if( langs ) {
		const key = line.shift();
		processString( key, line );
	} else {
		line.shift();
		langs = line;
		for( let i = 0; i < langs.length; i += 1 ) {
			if( ! projectLangs[ langs[ i ] ] ) {
				log( 'error', 'Missing language', langs[ i ], projectLangs );
				missingLangs[ langs[ i ] ] = true;
			}
		}
		initMissingLog();
	}
}

function initMissingLog( ){
	if( ! missingLog ) {
		return;
	}
	missingLog = csvWriter( { headers: arrayClonePrepend( 'Key', langs ) } );
	missingLog.pipe( fs.createWriteStream( argv.missing ) );
}

function receiveProject( res ) {

	projectLangs[ res.project.source_locale.code ] = true;

	const targetLocales = res.project.target_locales;

	for( let i = 0; i < targetLocales.length; i += 1 ) {
		projectLangs[ targetLocales[ i ].code ] = true;
	}

	fs.createReadStream( argv.input ).pipe( parser );
}


checkArgv();

log( 'debug', 'All good - arguments parsed', argv );

wtiApiUrl = WTI_BASE_URL + argv.project;

parser.on( 'data', receiveLine );

if( argv.missing ) {
	missingLog = true;
}

if( argv.log ) {
	errorLog = fs.createWriteStream( argv.log, { flags: 'a' } );
}

if( argv.logLevel && LOG_LEVELS[ argv.logLevel.toLowerCase() ] ) {
	logMinimumLevel = argv.logLevel.toLowerCase();
}
log( 'debug', 'Logging to console at level ' + logMinimumLevel );

wtiFetch( '', {}, 'GET', receiveProject );
