# CSV-WTI

Upload CSV file to WTI. All strings are checked before uploading; if the string is already identical, no change is made.

## CSV structure
The structure of the CSV file is supposed to be as follows:

- The first cell is ignored, following cells are supposed to contain the language codes for the strings uploaded.
- All other lines are to contain the key of the string in the first column, with translations in the rest of the columns.
- The second column must (because of WTI API limitations) be for the SOURCE language of the project.

## Options

The script accepts the following options:

- `--help` or `-?`: show usage information
- `--project #key#`: the private (write-enabled) API key - see WTI project settings for this one
- `--input #csvfile#`: the source csv file
- `--missing #csvfile#`: optional - if present, strings that were not found are logged to this CSV file.
- `--log #logfile#`: optional - if present, all log messages are appended to this file (which is created it does not exist).
- `--logLevel #level`: optional - if present, logging to console is limited to this level and above. Valid log levels are: `emergency`, `alert`, `critical`, `error`, `warning` (or `warn`), `notice`, `log`, `info` and `debug`, in decreasing order of priority.
